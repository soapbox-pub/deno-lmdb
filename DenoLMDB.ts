import lmdb from 'npm:lmdb@^3.0.3';

export class DenoLMDB implements Deno.Kv {
  private db: lmdb.RootDatabase;

  constructor(path: string) {
    this.db = lmdb.open({ path });
  }

  async get<T = unknown>(key: Deno.KvKey): Promise<Deno.KvEntryMaybe<T>> {
    const entry = this.db.getEntry(key as lmdb.Key);
    return {
      key,
      value: entry?.value ?? null,
      versionstamp: entry ? '0' : null,
    };
  }

  // @ts-ignore Can't make the types match for some reason.
  async getMany<T extends readonly unknown[]>(
    keys: readonly [...{ [K in keyof T]: Deno.KvKey }],
  ): Promise<{ [K in keyof T]: Deno.KvEntryMaybe<T[K]> }> {
    const promises = keys.map((key) => this.get(key));
    // deno-lint-ignore no-explicit-any
    return Promise.all(promises) as any;
  }

  async set(key: Deno.KvKey, value: unknown): Promise<Deno.KvCommitResult> {
    const ok = await this.db.put(key as lmdb.Key, value);
    if (!ok) {
      return Promise.reject({ ok });
    }
    return { ok, versionstamp: '0' };
  }

  async delete(key: Deno.KvKey): Promise<void> {
    await this.db.remove(key as lmdb.Key);
  }

  list<T = unknown>(selector: Deno.KvListSelector, options?: Deno.KvListOptions): Deno.KvListIterator<T> {
    throw new Error('Method not implemented.');
  }

  enqueue(
    value: unknown,
    options?: { delay?: number; keysIfUndelivered?: Deno.KvKey[]; backoffSchedule?: number[] },
  ): Promise<Deno.KvCommitResult> {
    throw new Error('Method not implemented.');
  }

  listenQueue(handler: (value: any) => void | Promise<void>): Promise<void> {
    throw new Error('Method not implemented.');
  }

  atomic(): Deno.AtomicOperation {
    throw new Error('Method not implemented.');
  }

  // @ts-ignore Can't make the types match for some reason.
  watch<T extends readonly unknown[]>(
    keys: readonly [...{ [K in keyof T]: Deno.KvKey }],
  ): ReadableStream<{ [K in keyof T]: Deno.KvEntryMaybe<T[K]> }> {
    throw new Error('Method not implemented.');
  }

  close(): void {
    this.db.close();
  }

  commitVersionstamp(): symbol {
    return Symbol.for('0');
  }

  [Symbol.dispose](): void {
    return;
  }
}
